<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Textarea | CoWaBoo</title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="http://bootswatch.com/paper/bootstrap.min.css">
		<link rel="stylesheet" href="widearea.css">
		<style type="text/css">
				textarea {
					width: 100%;
				}
				#story {
					resize: none;
					box-sizing: content-box;
				}
				.widearea-icons {
					z-index: 1!important;
				}
				.loading {
				    margin-left: 60px;
				    width: 120px;
				    display: none;
				}
				.diigo-tags a {
					margin-right: 5px;
				}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="header" style="text-align: center;">
				<h1>
				<a href="">Story generator</a>
				</h1>
			</div>
			<hr/>
			<div class="row">
				<div class="col-md-2">
					<ul class="nav nav-pills nav-stacked">
						<li><button class="btn btn-primary" data-toggle="modal" data-target="#diigoTagModal">Find tags</button></li>
						<li><button class="btn btn-primary" data-toggle="modal" data-target="#diigoBookmarkModal">Find bookmarks</button></li>
						<li><button class="btn btn-primary" data-toggle="modal" data-target="#zoteroGroupModal">Find groups</button></li>
						<li><button class="btn btn-primary" data-toggle="modal" data-target="#zoteroUserModal">Find users</button></li>
						<li><button class="btn btn-primary" data-toggle="modal" data-target="#wikipediaArticleModal">Find articles</button></li>
					</ul>
				</div>
				<div class="col-md-9">
					<textarea rows="3" data-widearea="enable" name="story" id="story" placeholder="Write your story here..." value=""></textarea>
				</div>
				<div class="col-md-1">
					<ul class="nav nav-pills nav-stacked">
						<li><button class="btn btn-success" data-toggle="modal" data-target="#storyModal">Post your story</button></li>
					</ul>
				</div>
			</div>
		</div>
		
		<!-- storyModal -->
		<div class="modal fade" id="storyModal" tabindex="-1" role="dialog" aria-labelledby="storyModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Post your story</h4>
					</div>
					<form class="" role="search" id="storyForm">
						<div class="modal-body">
							<div class="" style="padding: 5px;">
								<div class="row">
									<div class="form-group">
										<label for="storyTags">The tags of your story (separated by a comma):</label>
										<input value="" required style="padding: 5px;" type="text" class="form-control" id="storyTags" name="storyTags" placeholder="Enter the tags of this story"/>
									</div>
									<div class="form-group">
										<label for="storyDico">The dictionary of your story</label>
										<input value="" required style="padding: 5px;" type="text" class="form-control" id="storyDico" name="storyDico" placeholder="This story dictionnary"/>
									</div>
								</div>
								<div class="row">
									<div class="loading"><img src="ring.gif" alt="loading..."></div>								
									<div class="col-md-12 content"></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="insertIt btn btn-primary">Post your story to the dictionnary</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- diigoTagModal -->
		<div class="modal fade" id="diigoTagModal" tabindex="-1" role="dialog" aria-labelledby="diigoTagModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="diigoTagModalLabel">Diigo Tags</h4>
					</div>
					<div class="modal-body">
						<div class="">
							<div class="row">
								<form class="navbar-form navbar-left" role="search" id="diigoTagTagForm">
									<div class="form-group">
										<input type="text" class="form-control tagInput" id="diigoTagTag" placeholder="Enter a tag">
									</div>
									<button type="submit" class="btn btn-default">Search diigo related tags</button>
								</form>
							</div>
							<div class="row">
								<div class="loading"><img src="ring.gif" alt="loading..."></div>
								<div class="col-md-12 content"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="insertIt btn btn-primary" disabled>Insert selected bookmarks</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- diigoBookmarkModal -->
		<div class="modal fade" id="diigoBookmarkModal" tabindex="-1" role="dialog" aria-labelledby="diigoBookmarkModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="diigoBookmarkModalLabel">Diigo Bookmarks</h4>
					</div>
					<div class="modal-body">
						<div class="">
							<div class="row">
								<form class="navbar-form navbar-left" role="search" id="diigoBookmarkTagForm">
									<div class="form-group">
										<input type="text" class="form-control tagInput" id="diigoBookmarkTag" placeholder="Enter a tag">
									</div>
									<button type="submit" class="btn btn-default">Search diigo bookmarks</button>
								</form>
							</div>
							<div class="row">
								<div class="loading"><img src="ring.gif" alt="loading..."></div>
								<div class="col-md-12 content"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="insertIt btn btn-primary" disabled>Insert selected bookmarks</button>
					</div>
				</div>
			</div>
		</div>

		<!-- zoteroGroupModal -->
		<div class="modal fade" id="zoteroGroupModal" tabindex="-1" role="dialog" aria-labelledby="zoteroGroupModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="zoteroGroupModalLabel">Zotero Groups</h4>
					</div>
					<div class="modal-body">
						<div class="">
							<div class="row">
								<form class="navbar-form navbar-left" role="search" id="zoteroGroupTagForm">
									<div class="form-group">
										<input type="text" class="form-control tagInput" id="zoteroGroupTag" placeholder="Enter a tag">
									</div>
									<button type="submit" class="btn btn-default">Search Zotero groups</button>
								</form>
							</div>
							<div class="row">
								<div class="loading"><img src="ring.gif" alt="loading..."></div>
								<div class="col-md-12 content"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="insertIt btn btn-primary" disabled>Insert selected groups</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- zoteroUserModal -->
		<div class="modal fade" id="zoteroUserModal" tabindex="-1" role="dialog" aria-labelledby="zoteroUserModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="zoteroUserModalLabel">Zotero Users</h4>
					</div>
					<div class="modal-body">
						<div class="">
							<div class="row">
								<form class="navbar-form navbar-left" role="search" id="zoteroUserTagForm">
									<div class="form-group">
										<input type="text" class="form-control tagInput" id="zoteroUserTag" placeholder="Enter a tag">
									</div>
									<button type="submit" class="btn btn-default">Search Zotero users</button>
								</form>
							</div>
							<div class="row">
								<div class="loading"><img src="ring.gif" alt="loading..."></div>
								<div class="col-md-12 content"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="insertIt btn btn-primary" disabled>Insert selected users</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- wikipediaArticleModal -->
		<div class="modal fade" id="wikipediaArticleModal" tabindex="-1" role="dialog" aria-labelledby="wikipediaArticleModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="wikipediaArticleModalLabel">Wikipedia articles</h4>
					</div>
					<div class="modal-body">
						<div class="">
							<div class="row">
								<form class="navbar-form navbar-left" role="search" id="wikipediaArticleTagForm">
									<div class="form-group">
										<input type="text" class="form-control tagInput" id="wikipediaArticleTag" placeholder="Enter a tag">
									</div>
									<button type="submit" class="btn btn-default">Search Wikipedia articles</button>
								</form>
							</div>
							<div class="row">
								<div class="loading"><img src="ring.gif" alt="loading..."></div>
								<div class="col-md-12 content"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="insertIt btn btn-primary" disabled>Insert selected articles</button>
					</div>
				</div>
			</div>
		</div>

		<script src="widearea.js" type="text/javascript" charset="utf-8" async defer></script>
		<script src="autogrow.js" type="text/javascript" charset="utf-8" async defer></script>
		<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>

		<script type="text/javascript">
			var autoGrowTextarea;
			var items = [];
			var tags = [];

			document.onreadystatechange = function(){
				if(document.readyState == "complete"){
					wideArea().setOptions({
						'defaultColorScheme': "dark",
						'closeIconLabel': "Exit"
					});
					$('#story').val(localStorage.getItem('WDATA-1'));
					$('#storyDico').val(localStorage.getItem('storyDico'));
					if (!$('#storyDico').val()) {
						$('#storyDico').val('cowaboo');
					}
					var maxLines = 20,
					textarea = document.getElementById('story');
					autoGrowTextarea = new Autogrow(textarea, maxLines);
					autoGrowTextarea.autogrowFn();
				}
			}

			var setSelectItAction = function() {
				$('.selectIt').click(function(e) {
					e.preventDefault();
					if (this.dataset.isselected == 0) {
						this.dataset.isselected = 1;
						$(this).html('Unselect');
						$(this).removeClass('btn-primary');
						$(this).addClass('btn-danger');
					} else {
						this.dataset.isselected = 0;
						$(this).html('Select');
						$(this).addClass('btn-primary');
						$(this).removeClass('btn-danger');
					}
				});
			}

			var setModal = function(actionName, url, max, getITemsCallback, titleCallback, descriptionCallback, classTitle, createStringCallback, insertItCallback) {
				items[actionName] = [];
				tags[actionName] = '';
				$('#'+actionName+'TagForm').submit(function(e) {
					e.preventDefault();
					var tag = $('#'+actionName+'Tag').val();
					tags[actionName] = tag;
					if (!tag) {
						return false;
					}

					$('#'+actionName+'TagForm input').attr('disabled', 'disabled');
					// $('#'+actionName+'TagForm .btn').attr('disabled', 'disabled');
					$('.loading').show();
					
					$.ajax({
						url: url+tag,
					    success: function(data) {
					    	items[actionName] = getITemsCallback(data);
							$('#'+actionName+'TagForm input').attr('disabled', false);
							$('#'+actionName+'TagForm .btn').attr('disabled', false);
							$('.loading').hide();

							html = '';
							$(items[actionName]).each(function(key, item) {
								html += '<div class="'+classTitle+'">'
									+'<div class="panel panel-default">'
										+'<div class="panel-heading">'
											+'<h3 class="panel-title">'
												+'<button data-isselected="0" data-id="'+key+'" class="selectIt btn btn-sm btn-primary" href="">SELECT</button>'
												+' | '+titleCallback(item)
											+'</h3>'
										+'</div>'
										+'<div class="panel-body">'
											+descriptionCallback(item)
										+'</div>'
									+'</div>'
								+'</div>';
	    						return key<(max - 1);
							});

							$('#'+actionName+'Modal .content').html(html);
							$('#'+actionName+'Modal .insertIt').attr('disabled', false);

							setSelectItAction();
						}
					});
				});
				
				$('#'+actionName+'Modal .insertIt').click(function(e) {
					e.preventDefault();
					var values = $('#'+actionName+'Modal .selectIt[data-isselected=1]');
					var totalSize = $(values).length;
					$(values).each(function(key, item, b) {
						var item = items[actionName][item.dataset.id];
						var html = createStringCallback(item, key, totalSize);
						$('#story').val($('#story').val()+html);
						localStorage.setItem('WDATA-1', $('#story').val());

						// $('#story').val($('#story').val()+html);
					});

					if (insertItCallback) {
						insertItCallback(values, items[actionName]);
					}
					$('#'+actionName+'Modal').modal('toggle');
					$('#'+actionName+'Modal .content').html('');
					autoGrowTextarea.autogrowFn();
				});
			};

			setModal('diigoTag'
				, 'http://stadja.net:81/rest/cowaboo/tags/infos?tag_services=diigo&app_name=cowaboo&tag='
				, 12
				, function(data) {
					return data.diigo;
				},function(item) {
					return item;
				}, function(item) {
					return '';
				}, 'col-md-3'
				, function(item, key, totalSize) {
					$('#storyTags').val();
					var label = '';
					if (key == 0) {
						label = '\nTags related to "'+tags['diigoTag']+'": ';
					}
					label += '\n - '+item;
					if (totalSize == (key + 1)) {
						label += '\n';
					}
					return label;
				}, function(values, items) {
					var tags = $('#diigoTagTag').val();
					$(values).each(function(key, item) {
						tags += ','+items[item.dataset.id];
					});
					$('#storyTags').val(tags);
					$('.tagInput').val(tags);
				}
			);

			setModal('diigoBookmark'
				, 'http://stadja.net:81/rest/cowaboo/tags/bookmarks?bookmark_services=diigo&app_name=cowaboo&tag='
				, 20
				, function(data) {
					return data.diigo;
				},function(item) {
					return '<a target="_blank" href="'+item.link+'">'+item.title+'</a>';
				}, function(item) {
					return item.description;
				}, 'col-md-6'
				, function(item, key, totalSize) {
					var label = '';
					if (key == 0) {
						label = '\nBookmarks related to "'+tags['diigoBookmark']+'": ';
					}
					label += '\n - '+item.title+'\n   '+item.link;
					if (totalSize == (key + 1)) {
						label += '\n';
					}
					return label;
				}
			);

			setModal('zoteroGroup'
				, 'http://stadja.net:81/rest/cowaboo/tags/groups?group_services=zotero&app_name=cowaboo&tag='
				, 6
				, function(data) {
					return data.zotero;
				}, function(item) {
					return '<a target="_blank" href="'+item.url+'">'+item.name+'</a>';
				}, function(item) {
					return item.info;
				}, 'col-md-6'
				, function(item, key, totalSize) {
					var label = '';
					if (key == 0) {
						label = '\nGroups related to "'+tags['zoteroGroup']+'": ';
					}
					label += '\n - '+item.name+'\n   '+item.url;
					if (totalSize == (key + 1)) {
						label += '\n';
					}
					return label;
				}
			);

			setModal('zoteroUser'
				, 'http://stadja.net:81/rest/cowaboo/tags/users?user_services=zotero&app_name=cowaboo&tag='
				, 8
				, function(data) {
					return data.zotero;
				}, function(item) {
					return '<a target="_blank" href="'+item.url+'">'+item.name+'</a>';
				}, function(item) {
					return item.info;
				}, 'col-md-3'
				, function(item, key, totalSize) {
					var label = '';
					if (key == 0) {
						label = '\nUsers related to "'+tags['zoteroUser']+'": ';
					}
					label += '\n - '+item.name+'\n   '+item.url;
					if (totalSize == (key + 1)) {
						label += '\n';
					}
					return label;
				}
			);

			setModal('wikipediaArticle'
				, 'http://stadja.net:81/rest/cowaboo/tags/infos?tag_services=wikipedia&app_name=cowaboo&tag='
				, 20
				, function(data) {
					return data.wikipedia.articles;
				}, function(item) {
					return '<a target="_blank" href="'+item.url+'">'+item.title+'</a>';
				}, function(item) {
					return item.snippet;
				}, 'col-md-6'
				, function(item, key, totalSize) {
					var label = '';
					if (key == 0) {
						label = '\nArticles related to "'+tags['wikipediaArticle']+'": ';
					}
					label += '\n - '+item.title+'\n   '+item.url;
					if (totalSize == (key + 1)) {
						label += '\n';
					}
					return label;
				}
			);

			$('#storyForm').submit(function(e) {
				e.preventDefault();
				var data = {};
				var group = encodeURIComponent($('#storyDico').val());
				var tags = encodeURIComponent($('#storyTags').val());
				var description = encodeURIComponent($('#story').val());

				var url = 'http://stadja.net:81/rest/cowaboo/ipfs?app_name=cowaboo';
				url += '&group='+group;
				url += '&tags='+tags;
				url += '&description='+description;

				$('.loading').show();

				$.ajax({
					url: url,
					method: 'POST',
					success: function(data) {
						$('#storyModal').modal('toggle');
						$('#storyTags').val('');
						wideArea().clearData(1);
						$('.loading').hide();
						alert('Yes ! Your story has been posted :)');
					}
				});
			});

			$('#story').bind('change keyup', function() {
				localStorage.setItem('WDATA-1', $('#story').val());
			});

			$('#storyDico').bind('change keyup', function() {
				localStorage.setItem('storyDico', $('#storyDico').val());
			});
		</script>
	</body>
</html>
